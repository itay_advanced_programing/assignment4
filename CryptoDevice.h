#pragma once

#include <cstdio>
#include <iostream>
#include <string.h>
#include <cstdlib>
#include "osrng.h"
#include "modes.h"
#define CRYPTOPP_ENABLE_NAMESPACE_WEAK 1
#include <md5.h>
#include <hex.h>

class CryptoDevice
{

public:
    std::string encryptAES(std::string);
    std::string decryptAES(std::string);
	std::string encryptMD5(std::string);
	//std::string decryptMD5(std::string);


private:
    byte key[CryptoPP::AES::DEFAULT_KEYLENGTH], iv[CryptoPP::AES::BLOCKSIZE];

};
